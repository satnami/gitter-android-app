Fix GitLab issue decorations opening in GitHub (404), https://gitlab.com/gitlab-org/gitter/webapp/issues/1985
(updates from upstream gitter-webapp assets)
