package im.gitter.gitter.network;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import im.gitter.gitter.R;

public abstract class ApiRequest<T> extends JsonRequest<T> {

    private final Api api;

    public ApiRequest(Context context, String path, Response.Listener<T> listener, Response.ErrorListener errorListener) {
        this(context, Method.GET, path, null, listener, errorListener);
    }

    public ApiRequest(Context context, int method, String path, JSONObject jsonRequest, Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(method, context.getResources().getString(R.string.api_host) + path, (jsonRequest == null) ? null : jsonRequest.toString(), listener, errorListener);

        this.api = new Api(context);
        setRetryPolicy(api.getRetryPolicy());
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return api.getHeaders();
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));
            T t = parseJsonInBackground(jsonString);
            return Response.success(t, HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException je) {
            return Response.error(new ParseError(je));
        }
    }

    /**
     * This allows you to safely parse the json string into a usable object in the background thread
     * so that the ui thread doesnt skip frames.
     *
     * @param jsonString from network
     * @return whatever object you want to use
     * @throws JSONException
     */
    protected abstract T parseJsonInBackground(String jsonString) throws JSONException;
}
